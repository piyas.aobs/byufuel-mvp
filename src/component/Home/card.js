import { Image, StyleSheet, Text, View } from "react-native";
import React from "react";
import Colors from "../../utils/Colors";
import moment from "moment";
import { Ionicons } from "@expo/vector-icons";
import Button from "../Button";
import { useNavigation } from "@react-navigation/native";
import { networkInstance } from "../../network";

const HomeCard = ({ item }) => {
  const navigation = useNavigation();

  const onPressButton = async () => {
    try {
      const formdata = new FormData();
      formdata.append("driver_id", "1");
      formdata.append("trip_id", item.id);
      const response = await networkInstance.post("trip/accept", formdata);
      console.log(response);

      navigation.push("Details", item);
    } catch (error) {
      navigation.navigate("Details", item);
      console.log(error);
    }
  };
  return (
    <View style={styles.item}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          width: "60%"
        }}
      >
        <View style={styles.iconContainer}>
          <Image source={{}} style={styles.icon} />
        </View>

        <View style={styles.textContainer}>
          <Text style={styles.title}>{item.supplier_name}</Text>
          <Text style={styles.subtitle}>Destination: {item.destination}</Text>
          <Text style={styles.date}>
            {moment(item.created_at).format("MMMM Do YYYY")}
          </Text>
        </View>
        <Ionicons name="chevron-forward" size={24} color="#999" />
      </View>

      {item.accepted == 0 ? (
        <Button onPress={() => onPressButton()} title={"Accept"} />
      ) : (
        <Button
          onPress={() => {
            navigation.navigate("Details", item);
          }}
          title={"Accepted"}
        />
      )}
    </View>
  );
};

export default HomeCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f8f9fa",
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  loadingText: {
    marginTop: 10,
    fontSize: 18,
    color: "#000"
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#fff",
    padding: 15,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    elevation: 5,
    marginVertical: 5
  },
  iconContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginRight: 15
  },
  icon: {
    width: 40,
    height: 40,
    backgroundColor: Colors.dark2,
    borderRadius: 10
  },
  textContainer: {
    flex: 1
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#333"
  },
  subtitle: {
    fontSize: 16,
    color: "#666",
    marginTop: 5
  },
  date: {
    fontSize: 14,
    color: "#999",
    marginTop: 10
  },
  separator: {
    height: 10
  }
});
