import React, { useState, useRef, useEffect } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Platform,
  PermissionsAndroid,
  Alert,
  ActivityIndicator
} from "react-native";
import { request, PERMISSIONS, RESULTS } from "react-native-permissions";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import Geolocation from "@react-native-community/geolocation";
import Tts from "react-native-tts";
import { GOOGLE_MAPS_APIKEY } from "../utils/keys";
import haversine from "haversine";
import { networkInstance } from "../network";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GEOFENCE_RADIUS = 500; // 50 meters

const MapScreen = ({ route, navigation }) => {
  const [currentLocation, setCurrentLocation] = useState(null);
  const [destination, setDestination] = useState(null);
  const [isNavigating, setIsNavigating] = useState(false);
  const [routeAnnounced, setRouteAnnounced] = useState(false); // State variable to track if the route has been announced
  const mapView = useRef(null);
  const locationWatcher = useRef(null);

  const { latitude, longitude, id } = route?.params;

  useEffect(() => {
    initializeMap();

    return () => {
      if (locationWatcher.current) {
        Geolocation.clearWatch(locationWatcher.current);
        locationWatcher.current = null;
      }
    };
  }, []);

  const initializeMap = async () => {
    setCurrentLocation(null);
    setDestination(null);
    setIsNavigating(false);
    setRouteAnnounced(false);
   

    setDestination({
      latitude: Number(latitude),
      longitude: Number(longitude)
    });

    const requestLocationPermission = async () => {
      if (Platform.OS === "android") {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "Location Access Required",
            message: "This app needs to access your location"
          }
        );
        if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
          Alert.alert(
            "Permission Denied",
            "Location permission is required to use this app."
          );
          return;
        }
      } else if (Platform.OS === "ios") {
        const status = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
        if (status !== RESULTS.GRANTED) {
          Alert.alert(
            "Permission Denied",
            "Location permission is required to use this app."
          );
          return;
        }
      }
      getLocation();
    };

    requestLocationPermission();
  };

  const getLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        console.log("Current position:", position);
        setCurrentLocation({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        });
      },
      (error) => {
        console.log("Error getting location:", error);
        Alert.alert("Error", "Failed to get current location");
      },
      { enableHighAccuracy: true, timeout: 20000 }
    );
  };

  const hasArrivedDestination = async () => {
    try {
      Tts.speak("You have arrived at your destination");
      const formdata = new FormData();
      formdata.append("driver_id", "1");
      formdata.append("trip_id", id);
      const response = await networkInstance.post(
        "triggerGeofenceEvent",
        formdata
      );
      if (response.status === 200) {
        navigation.reset({
          index: 0,
          routes: [{ name: "CompleteScreen" }]
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (isNavigating) {
      locationWatcher.current = Geolocation.watchPosition(
        (position) => {
          const newLocation = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          };
          setCurrentLocation(newLocation);
          mapView.current?.animateCamera({
            center: newLocation,
            zoom: 18,
            heading: position.coords.heading,
            pitch: 45
          });

          // Check if within geofence
          if (
            destination &&
            haversine(newLocation, destination, { unit: "meter" }) <=
              GEOFENCE_RADIUS
          ) {
             Geolocation.clearWatch(locationWatcher.current);
            
            hasArrivedDestination();
          }  
        },
        (error) => console.log(error),
        {
          enableHighAccuracy: true,
          distanceFilter: 10,
          interval: 10000,
          fastestInterval: 5000
        }
      );
    }  

    return () => {
      if (locationWatcher.current) {
        Geolocation.clearWatch(locationWatcher.current);
      }
    };
  }, [isNavigating]);

  const handleMapPress = (e) => {
    // setDestination(e.nativeEvent.coordinate);
  };

  const handleStartStopNavigation = () => {
    if (!currentLocation || !destination) {
      Alert.alert("Error", "Please select a destination");
      return;
    }

    setIsNavigating((prevState) => !prevState);

    if (!isNavigating) {
      Tts.speak("Navigation started.");
      setRouteAnnounced(false); // Reset the route announcement state
    
      mapView.current.animateCamera({
        center: currentLocation,
        zoom: 18,
        heading: 0,
        pitch: 0
      });
    } else {
      Tts.speak("Navigation stopped.");
    }
  };

  const renderDirections = () => {
    if (currentLocation && destination) {
      return (
        <MapViewDirections
          origin={currentLocation}
          destination={destination}
          apikey={GOOGLE_MAPS_APIKEY}
          strokeWidth={4}
          strokeColor="blue"
          optimizeWaypoints={true}
          onReady={(result) => {
            if (!routeAnnounced) {
              // Check if the route has already been announced
              console.log(`Distance: ${result.distance} km`);
              console.log(`Duration: ${result.duration} min.`);
              Tts.speak(
                `The distance is ${
                  result.distance
                } kilometers and it will take approximately ${Math.floor(
                  result.duration
                )} minutes.`
              );
              setRouteAnnounced(true); // Mark the route as announced
            }
            mapView.current.fitToCoordinates(result.coordinates, {
              edgePadding: {
                right: width / 20,
                bottom: height / 20,
                left: width / 20,
                top: height / 20
              }
            });
          }}
          onError={(errorMessage) => {
            console.log("GOT AN ERROR", errorMessage);
            Tts.speak("There was an error in calculating the route.");
            Alert.alert(
              "Error",
              "There was an error in calculating the route."
            );
          }}
        />
      );
    }
    return null;
  };

  return (
    <View style={styles.container}>
      {currentLocation ? (
        <MapView
          initialRegion={currentLocation}
          style={StyleSheet.absoluteFill}
          ref={mapView}
          onPress={handleMapPress}
        >
          <Marker coordinate={currentLocation} title="You are here" />
          {destination && (
            <Marker coordinate={destination} title="Destination" />
          )}
          {renderDirections()}
        </MapView>
      ) : (
        <ActivityIndicator size="large" color="#0000ff" />
      )}
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={[styles.button, isNavigating && styles.buttonStop]}
          onPress={handleStartStopNavigation}
        >
          <Text style={styles.buttonText}>
            {isNavigating ? "Stop" : "Start"}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  buttonContainer: {
    position: "absolute",
    bottom: 40,
    width: "90%",
    alignItems: "center"
  },
  button: {
    backgroundColor: "#007bff",
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 30,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  buttonStop: {
    backgroundColor: "#ff0000"
  },
  buttonText: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "bold"
  }
});

export default MapScreen;
