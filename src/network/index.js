import axios from "axios";

export const networkInstance = axios.create({
  baseURL: "https://byufueldemo.hyperlycheelabs.com/api",
  headers: {
    Accept: "application/json, text/plain, /",
    "Content-Type": "multipart/form-data"
  }
});
