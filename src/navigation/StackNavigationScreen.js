import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import MapScreen from "../screens/MapScreen";
import HomeScreen from "../screens/homeScreen";
import CompleteScreen from "../screens/CompleteScreen";

const Stack = createStackNavigator();

function StackNavigationScreen() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen
          options={{
            headerShown: true
          }}
          name="Details"
          component={MapScreen}
        />
        <Stack.Screen name="CompleteScreen" component={CompleteScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNavigationScreen;
