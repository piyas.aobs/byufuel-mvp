import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
 
import StackNavigationScreen from './src/navigation/StackNavigationScreen'

const App = () => {
  return (
     <StackNavigationScreen/>
  )
}

export default App

const styles = StyleSheet.create({})